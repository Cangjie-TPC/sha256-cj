# 库 / 模块 / 类设计介绍

## 描述
    本库是Cangjie语言实现的SHA-256/SHA-224哈希函数，支持UTF-8编码。

## class A design API



### 类API
```cangjie

// class API
public class Sha256 {
    public init()
    public func update(message: ?String): Sha256
    public func update(message: ?Array<UInt8>): Sha256
    public func finalize(): Unit
    public func hex(): String
    public func digest(): Array<Byte>
    public func arrayBuffer(): Array<Byte>
    public func array(): Array<Byte>
    public func toString(): String
}

public class Sha224 {
    public init()
    public func update(message: ?String): Sha256
    public func update(message: ?Array<UInt8>): Sha256
    public func finalize(): Unit
    public func hex(): String
    public func digest(): Array<Byte>
    public func arrayBuffer(): Array<Byte>
    public func array(): Array<Byte>
    public func toString(): String
}
```




## 展示示例

```cangjie
@Test
func TestChineseCharacters() {
    var input = "中文"
    var expect = "72726d8818f693066ceb69afa364218b692e62ea92b385782363780f47529c21"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}
```

```cangjie
@Test
func TestQuickBrownFoxDot() {
    var input = "The quick brown fox jumps over the lazy dog."
    var expect = "619cba8e8e05826e9b8c519c0a5c68f4fb653e8a3d8aa04bb2c8cd4c"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}

```