**# sha256-cj库**

**### 介绍**

本库是Cangjie语言实现的SHA-256/SHA-224哈希函数，支持UTF-8编码。

**### 1 easyreplace算法**

需求来源：三方库 用户：仓颉开发者 场景：提供一种仓颉语言实现的sha256算法

**##### 1.1 主要接口**

```
public class Sha256 {
    /**
     * 初始化SHA256哈希计算器
     * 设置初始哈希值和状态变量
     */
    public init()

    /**
     * 使用字符串更新哈希计算
     * 
     * @param message 输入需要计算哈希的字符串
     * @return 返回更新后的Sha256对象
     * @throws IllegalArgumentException 当输入为空时抛出异常
     */
    public func update(message: ?String): Sha256

    /**
     * 使用字节数组更新哈希计算
     *
     * @param message 输入需要计算哈希的字节数组
     * @return 返回更新后的Sha256对象
     * @throws IllegalArgumentException 当输入为空时抛出异常
     */
    public func update(message: ?Array<UInt8>): Sha256

    /**
     * 完成哈希计算
     * 处理最后的数据块并设置终止标志
     */
    public func finalize(): Unit

    /**
     * 获取十六进制格式的哈希值
     *
     * @return 返回64字符的十六进制哈希字符串
     */
    public func hex(): String

    /**
     * 获取哈希值的字节数组
     *
     * @return 返回32字节的哈希值数组
     */
    public func digest(): Array<Byte>

    /**
     * 获取哈希值的字节缓冲区
     *
     * @return 返回32字节的哈希值缓冲区
     */
    public func arrayBuffer(): Array<Byte>

    /**
     * 获取哈希值的字节数组
     *
     * @return 返回32字节的哈希值数组
     */
    public func array(): Array<Byte>

    /**
     * 获取十六进制格式的哈希值字符串
     *
     * @return 返回64字符的十六进制哈希字符串
     */
    public func toString(): String
}
```