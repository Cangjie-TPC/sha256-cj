<div align="center">
<h1>sha256-cj</h1>
</div>


<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.4-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-0.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化/毕业-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-HOS/Cloud-brightgreen" style="display: inline-block;" />
</p>

## 介绍

本库是Cangjie语言实现的SHA-256/SHA-224哈希函数，支持UTF-8编码。

### 项目特性

- 支持UTF-8编码
- 支持ASCII编码
- 支持数组编码
- 支持特殊长度编码

### 源码目录

```shell
.
|-- CHANGELOG.md
|-- LICENSE
|-- README.OpenSource
|-- README.md
|-- cjpm.lock
|-- cjpm.toml
|-- src
|   |-- main.cj
|   |-- sha224.cj
|   |-- sha224_test.cj
|   |-- sha256.cj
|   `-- sha256_test.cj
`-- test
    |-- HLT
    `-- LLT

```

- `doc` 文档目录，用于存API接口文档
- `src` 是库源码目录
- `test` 存放 HLT 测试用例、LLT 自测用例、FUZZ 测试用例和文档示例用例

### 接口说明

主要类和函数接口说明，详见 [API](./doc/feature_api.md)


## 使用说明
### 编译构建

描述具体的编译过程：

```shell
cpm build
```

### 功能示例

### SHA256

#### 1,ASCII编码

示例代码如下：

```cangjie
@Test
func TestQuickBrownFox() {
    var input = "The quick brown fox jumps over the lazy dog"
    var expect = "d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
```

执行结果如下：

```shell
[ PASSED ] CASE: TestQuickBrownFox
```

#### 2,UTF8编码

示例代码如下：

```cangjie
@Test
func TestChineseCharacters() {
    var input = "中文"
    var expect = "72726d8818f693066ceb69afa364218b692e62ea92b385782363780f47529c21"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}
```

执行结果如下：

```shell
[ PASSED ] CASE: TestChineseCharacters
```

#### 3,特殊长度编码

示例代码如下：

```cangjie
@Test
func TestSpecialLength60() {
    var input = "0123456780123456780123456780123456780123456780123456780"
    var expect = "5e6b963e2b6444dab8544beab8532850cef2a9d143872a6a5384abe37e61b3db"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}
```

执行结果如下：

```shell
[ PASSED ] CASE: TestSpecialLength60
```

#### 4,数组编码

示例代码如下：

```cangjie
@Test
func TestByteArray() {
    var input: Array<UInt8> = [211, 212]
    var expect = "182889f925ae4e5cc37118ded6ed87f7bdc7cab5ec5e78faef2e50048999473f"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}
```

执行结果如下：

```shell
[ PASSED ] CASE: TestByteArray
```

#### SHA224

示例代码如下：

```cangjie
@Test
func TestQuickBrownFoxDot() {
    var input = "The quick brown fox jumps over the lazy dog."
    var expect = "619cba8e8e05826e9b8c519c0a5c68f4fb653e8a3d8aa04bb2c8cd4c"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}
```

执行结果如下：

```shell
[ PASSED ] CASE: TestQuickBrownFoxDot
```

#### 2,UTF8编码

示例代码如下：

```cangjie
@Test
func TestChineseCharacters() {
    var input = "中文"
    var expect = "dfbab71afdf54388af4d55f8bd3de8c9b15e0eb916bf9125f4a959d4"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}
```

执行结果如下：

```shell
[ PASSED ] CASE: TestChineseCharacters
```

#### 3,特殊长度编码

示例代码如下：

```cangjie
@Test
func TestSpecialLength60() {
    var input = "0123456780123456780123456780123456780123456780123456780"
    var expect = "bc4a354d66f3cff4bc6dd6a88fbb0435cede7fd5fe94da0760cb1924"
    let sha256 = Sha256()
    sha256.update(input)
    var result = sha256.hex()
    @Expect(expect, result)
}
```

执行结果如下：

```shell
[ PASSED ] CASE: TestSpecialLength60
```

#### 4,数组编码

示例代码如下：

```cangjie
@Test
func TestLongByteArray() {
    var input: Array<UInt8> = [84, 104, 101, 32, 113, 117, 105, 99, 107, 32, 98, 114, 111, 119, 110, 32, 102, 111,
        120, 32, 106, 117, 109, 112, 115, 32, 111, 118, 101, 114, 32, 116, 104, 101, 32, 108, 97, 122, 121, 32, 100,
        111, 103]
    var expect = "730e109bd7a8a32b1cb9d9a09aa2325d2430587ddbc0c38bad911525"
    let sha224 = Sha224()
    sha224.update(input)
    var result = sha224.hex()
    @Expect(expect, result)
}

```

执行结果如下：

```shell
[ PASSED ] CASE: TestLongByteArray
```

#### 

## 约束与限制

在下述版本验证通过：

```
Cangjie Version: 0.57.3
```

## 开源协议
[MIT License](https://gitcode.com/Cangjie-TPC/sha256-cj/blob/develop/LICENSE)

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。